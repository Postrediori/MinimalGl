## Introduction

Rendering programs based on the projects by [Keith Lantz](https://keithlantz.net/) implented in C (C11 standard) & OpenGL 3.3.

## Prerequisites

Compiling and running the sample project requires CMake and C compiler.
Most of the dependencies needed are bundled with the repository. This includes:

* [glad](https://github.com/Dav1dde/glad) - OpenGL Function Loader.
* [glfw](https://github.com/glfw/glfw) - Windowing and Input.
* [HandmadeMath](https://github.com/HandmadeMath/Handmade-Math) - OpenGL Mathematics.
* [stb](https://github.com/nothings/stb) - Image loader.
* [inih](https://github.com/benhoyt/inih) - INI parser.

## Environment Setup

### Debian-based Systems

The following instructions apply to:

* Ubuntu 22.04, 20.04, 18.04, 16.04

```
apt-get install \
    build-essential \
    cmake \
    xorg-dev \
    libgl1-mesa-dev
```

### RedHat-based Systems

The following instructions apply to:

* Fedora 22 and higher

```
dnf install \
    cmake \
    gcc-c++ \
    mesa-libGL-devel \
    libXrandr-devel \
    libXinerama-devel \
    libXcursor-devel \
    libXi-devel
```

## Cloning Repository

Cloning the repository requires passing the `--recursive` flag to load dependencies

```
git clone --recursive https://gitlab.com/Postrediori/OpenGlRendering.git
cd OpenGlRendering
```

## Building Projects

The program is built with the commands below. CMake requires the directory 
with the main project's `CMakeLists.txt` file as an argument. Then the CMake 
creates the build files for the GNU make which build an executable.

```
mkdir build && cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make
```

Compiled executables will be installed to the `bundle` subdirectory in the main source tree:

```
make install
```


## Running Projects

### Skybox

```
cd bundle/Skybox
./Skybox
```

![Skybox screenshot](images/skybox.png)

## Links

* [Rendering a skybox using a cube map with OpenGL and GLSL](https://www.keithlantz.net/2011/10/rendering-a-skybox-using-a-cube-map-with-opengl-and-glsl/).
