set(STB_INCLUDE_DIR ${CMAKE_SOURCE_DIR}/3rdparty/stb)

add_library(stb_image INTERFACE)
target_include_directories(stb_image INTERFACE ${STB_INCLUDE_DIR})

set(STB_LIBRARY stb_image)
