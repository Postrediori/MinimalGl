set(INIH_INCLUDE_DIR ${CMAKE_SOURCE_DIR}/3rdparty/inih)
file(GLOB INIH_SOURCES ${INIH_INCLUDE_DIR}/ini.c)
file(GLOB INIH_HEADERS ${INIH_INCLUDE_DIR}/ini.h)

add_library(inih STATIC ${INIH_HEADERS} ${INIH_SOURCES})

target_include_directories(inih INTERFACE ${INIH_INCLUDE_DIR})

set_target_properties(glad PROPERTIES LINKER_LANGUAGE C)
set_target_properties(glad PROPERTIES FOLDER 3rdparty)

set(INIH_LIBRARIES inih)
