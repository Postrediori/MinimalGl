#include "pch.h"
#include "Logger.h"
#include "GraphicsLogger.h"

static const char* GetErrorDescription(GLenum err) {
    switch (err) {
    case GL_INVALID_ENUM: return "INVALID_ENUM";
    case GL_INVALID_VALUE: return "INVALID_VALUE";
    case GL_INVALID_OPERATION: return "INVALID_OPERATION";
    case GL_INVALID_FRAMEBUFFER_OPERATION: return "INVALID_FRAMEBUFFER_OPERATION";
    case GL_OUT_OF_MEMORY: return "OUT_OF_MEMORY";
    case GL_STACK_UNDERFLOW: return "STACK_UNDERFLOW";
    case GL_STACK_OVERFLOW: return "STACK_OVERFLOW";
    default: return "Unknown error";
    }
}

void GraphicsUtils_LogOpenGLError(const char* file, int line) {
    GLenum err = glGetError();

    if (err != GL_NO_ERROR) {
        LOG_ERROR("OpenGL Error in file %s line %d : %s", file, line, GetErrorDescription(err));
    }
}
