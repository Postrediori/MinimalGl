#include "pch.h"
#include "Utils.h"
#include "Logger.h"
#include "GraphicsLogger.h"
#include "ShaderUtils.h"

enum { BufferLen = 1024 };

static char* GetShaderInfo(GLuint shader, char* str, size_t max_len) {
    GLsizei length = 0;

    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, (GLint*)(&length)); LOGOPENGLERROR();
    if (length == 0) {
        strncpy(str, "", max_len);
        return str;
    }

    length = MIN(length, max_len);
    glGetShaderInfoLog(shader, length, NULL, str); LOGOPENGLERROR();

    return str;
}

static char* GetProgramInfo(GLuint program, char* str, size_t max_len) {
    GLsizei length = 0;

    glGetProgramiv(program, GL_INFO_LOG_LENGTH, (GLint*)(&length)); LOGOPENGLERROR();
    if (length == 0) {
        strncpy(str, "", max_len);
        return str;
    }

    length = MIN(length, max_len);
    glGetProgramInfoLog(program, length, NULL, str); LOGOPENGLERROR();

    return str;
}

static void ReleaseProgram(GLuint program, GLuint vertex, GLuint fragment) {
    if (vertex) {
        glDeleteShader(vertex); LOGOPENGLERROR();
    }

    if (fragment) {
        glDeleteShader(fragment); LOGOPENGLERROR();
    }

    if (program) {
        glDeleteProgram(program); LOGOPENGLERROR();
    }
}

GLuint CreateShaderFromSources(const char* vertexSource, const char* fragmentSource) {
    LOG_DEBUG("Vertex Shader    : %zu characters", strlen(vertexSource));
    LOG_DEBUG("Fragment Shader  : %zu characters", strlen(fragmentSource));

    GLint result = 0;
    GLuint sProgram = 0;
    GLuint vShader = 0;
    GLuint fShader = 0;

    char buffer[BufferLen] = {0};

    vShader = glCreateShader(GL_VERTEX_SHADER); LOGOPENGLERROR();
    if (vShader == 0) {
        LOG_ERROR("Unable to Create Vertex Shader");
        goto error;
    }

    fShader = glCreateShader(GL_FRAGMENT_SHADER); LOGOPENGLERROR();
    if (!fShader) {
        LOG_ERROR("Unable to Create Fragment Shader");
        goto error;
    }

    glShaderSource(vShader, 1, &vertexSource, NULL); LOGOPENGLERROR();
    glCompileShader(vShader); LOGOPENGLERROR();
    glGetShaderiv(vShader, GL_COMPILE_STATUS, &result); LOGOPENGLERROR();
    if (result == 0) {
        LOG_ERROR("Vertex Shader Error : %s", GetShaderInfo(vShader, buffer, BufferLen));
        goto error;
    }

    glShaderSource(fShader, 1, &fragmentSource, NULL); LOGOPENGLERROR();
    glCompileShader(fShader); LOGOPENGLERROR();
    glGetShaderiv(fShader, GL_COMPILE_STATUS, &result); LOGOPENGLERROR();
    if (!result) {
        LOG_ERROR("Fragment Shader Error : %s", GetShaderInfo(fShader, buffer, BufferLen));
        goto error;
    }

    sProgram = glCreateProgram(); LOGOPENGLERROR();
    if (sProgram == 0) {
        LOG_ERROR("Unable to Create Program");
        goto error;
    }

    glAttachShader(sProgram, vShader); LOGOPENGLERROR();
    glAttachShader(sProgram, fShader); LOGOPENGLERROR();

    glLinkProgram(sProgram); LOGOPENGLERROR();

    glGetProgramiv(sProgram, GL_LINK_STATUS, &result); LOGOPENGLERROR();
    if (result == 0) {
        LOG_ERROR("Linking Shader Error : %s", GetProgramInfo(sProgram, buffer, BufferLen));
        goto error;
    }

    glDeleteShader(vShader); LOGOPENGLERROR();
    glDeleteShader(fShader); LOGOPENGLERROR();

    return sProgram;

error:
    ReleaseProgram(sProgram, vShader, fShader);
    return 0;
}
