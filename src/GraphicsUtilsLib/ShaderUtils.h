#pragma once

GLuint CreateShaderFromSources(const char* vertexSource, const char* fragmentSource);
