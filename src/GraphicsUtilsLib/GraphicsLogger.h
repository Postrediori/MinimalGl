#pragma once

#ifdef NDEBUG
# define LOGOPENGLERROR() ((void)0)
#else
# define LOGOPENGLERROR() GraphicsUtils_LogOpenGLError(__FILE__,__LINE__)
#endif

void GraphicsUtils_LogOpenGLError(const char *file, int line);
