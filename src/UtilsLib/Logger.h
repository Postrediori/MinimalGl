#pragma once

void LogError(const char* fname, int n, const char* message, ...);
void LogInfo(const char* fname, int n, const char* message, ...);
void LogDebug(const char* fname, int n, const char* message, ...);

#define LOG_ERROR(message, ...) LogError(__FUNCTION__,__LINE__, message, ## __VA_ARGS__)
#define LOG_INFO(message, ...) LogInfo(__FUNCTION__,__LINE__, message, ## __VA_ARGS__)
#define LOG_DEBUG(message, ...) LogDebug(__FUNCTION__,__LINE__, message, ## __VA_ARGS__)
