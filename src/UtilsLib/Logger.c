#include "pch.h"
#include "Utils.h"
#include "Logger.h"

enum { BufferLen = 32 };

static const char ErrorTag[] = "ERROR";
static const char InfoTag[] = "INFO";
static const char DebugTag[] = "DEBUG";

static void LogCurrentTimestamp(FILE* f) {
    struct timespec now = { 0 };
    char buffer[BufferLen] = { 0 };
    if (timespec_get(&now, TIME_UTC) != 0) {
        strftime(buffer, BufferLen - 1, "%Y-%m-%d %H:%M:%S", localtime((const time_t*)(&now)));
        fprintf(f, "%s.%03ld ", buffer, now.tv_nsec / 1000000L);
    }
}

static void LogFormat(FILE* f, const char* tag, const char* fname, int n, const char* message, va_list args) {
    LogCurrentTimestamp(f);
    fprintf(f, "%5s [%s@%d] ", tag, fname, n);
    vfprintf(f, message, args);
    fprintf(f, "\n");
}

void LogError(const char* fname, int n, const char* message, ...) {
    va_list args;
    va_start(args, message);
    LogFormat(stderr, ErrorTag, fname, n, message, args);
    va_end(args);
}

void LogInfo(const char* fname, int n, const char* message, ...) {
    va_list args;
    va_start(args, message);
    LogFormat(stdout, InfoTag, fname, n, message, args);
    va_end(args);
}

#ifdef NDEBUG
void LogDebug(const char* fname, int n, const char* message, ...) {
    UNUSED(fname);
    UNUSED(n);
    UNUSED(message);
}
#else
void LogDebug(const char* fname, int n, const char* message, ...) {
    va_list args;
    va_start(args, message);
    LogFormat(stdout, DebugTag, fname, n, message, args);
    va_end(args);
}
#endif
