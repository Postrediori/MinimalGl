#ifndef STB_IMAGE_IMPLEMENTATION
# define STB_IMAGE_IMPLEMENTATION
# include <stb_image.h>
#endif

#include "pch.h"
#include "Utils.h"
#include "Logger.h"
#include "GraphicsLogger.h"
#include "ShaderUtils.h"
#include "Skybox.h"


/*****************************************************************************
 * Shader sources
 ****************************************************************************/

static const char VertexShaderSource[] = "#version 330 core\n\
in vec3 coord;\n\
out vec3 tex_coord;\n\
uniform mat4 model, view, projection;\n\
void main(void) {\n\
    mat4 mvp = projection * view * model;\n\
    gl_Position = mvp * vec4(coord, 1.);\n\
    tex_coord = coord;\n\
}";

static const char FragmentShaderSource[] = "#version 330 core\n\
in vec3 tex_coord;\n\
out vec4 fragColor;\n\
uniform samplerCube cubemap;\n\
void main(void) {\n\
    fragColor=texture(cubemap, tex_coord); \n\
}";


/*****************************************************************************
 * Buffer data
 ****************************************************************************/

static const float CubeVerticesData[] = {
    -1.0,  1.0,  1.0,
    -1.0, -1.0,  1.0,
    1.0, -1.0,  1.0,
    1.0,  1.0,  1.0,
    -1.0,  1.0, -1.0,
    1.0,  1.0, -1.0,
    1.0, -1.0, -1.0,
    -1.0, -1.0, -1.0,
};

static const GLushort CubeIndicesData[] = {
    0, 1, 2,  2, 0, 3,
    4, 5, 6,  6, 4, 7,
    0, 3, 5,  5, 0, 4,
    1, 7, 6,  6, 1, 2,
    2, 6, 5,  5, 2, 3,
    0, 4, 7,  7, 0, 1,
};


/*****************************************************************************
 * Cubemap utils
 ****************************************************************************/

int InitCubemapSide(GLuint side, const char* filename) {
    int w = 0, h = 0, channels = 0;
    stbi_uc* data = stbi_load(filename, &w, &h, &channels, 0);
    if (!data) {
        LOG_ERROR("Unable to Load Cubemap Side Image From File %s", filename);
        return -1;
    }

    const GLuint channelsFlag = (channels == 4 ? GL_RGBA : GL_RGB);
    glTexImage2D(side, 0, GL_RGBA, w, h, 0, channelsFlag, GL_UNSIGNED_BYTE, data);

    stbi_image_free(data);

    return 0;
}

static GLuint InitCubemapTexture(SkyboxConfiguration config) {
    GLuint texture = 0;

    glGenTextures(1, &texture);
    if (texture == 0) {
        LOG_ERROR("Unable to Create Cubemap Texture");
        return 0;
    }

    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    if (InitCubemapSide(GL_TEXTURE_CUBE_MAP_POSITIVE_X, config.xpos) != 0) {
        return 0;
    }
    if (InitCubemapSide(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, config.xneg) != 0) {
        return 0;
    }
    if (InitCubemapSide(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, config.ypos) != 0) {
        return 0;
    }
    if (InitCubemapSide(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, config.yneg) != 0) {
        return 0;
    }
    if (InitCubemapSide(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, config.zpos) != 0) {
        return 0;
    }
    if (InitCubemapSide(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, config.zneg) != 0) {
        return 0;
    }

    return texture;
}


/*****************************************************************************
 * Implementations
 ****************************************************************************/

int Skybox_Init(Skybox* p, SkyboxConfiguration config) {
    assert(p);

    p->cubemapTex = InitCubemapTexture(config);
    if (p->cubemapTex == 0) {
        LOG_ERROR("Failed to load cubemap textures");
        return -1;
    }

    // Create shader program
    p->program = CreateShaderFromSources(VertexShaderSource, FragmentShaderSource);
    if (p->program == 0) {
        LOG_ERROR("Failed to create shader program");
        return -1;
    }

    // Get uniform locations
    p->uModel = glGetUniformLocation(p->program, "model"); LOGOPENGLERROR();
    p->uView = glGetUniformLocation(p->program, "view"); LOGOPENGLERROR();
    p->uProjection = glGetUniformLocation(p->program, "projection"); LOGOPENGLERROR();
    p->uCubeMap = glGetUniformLocation(p->program, "cubemap"); LOGOPENGLERROR();
    if (p->uModel == -1 || p->uView == -1 || p->uProjection == -1 ||
            p->uCubeMap == -1) {
        LOG_ERROR("Invalid shader program");
        return -1;
    }

    // Create vertex array object
    glGenVertexArrays(1, &p->vao); LOGOPENGLERROR();
    if (p->vao == 0) {
        LOG_ERROR("Failed to create vertex array object");
        return -1;
    }

    // Bind vertex array and create buffers
    glBindVertexArray(p->vao); LOGOPENGLERROR();

    glGenBuffers(1, &p->vbo); LOGOPENGLERROR();
    if (p->vbo == 0) {
        LOG_ERROR("Unable to create Vertex Buffer Object for Skybox Vertices");
        return -1;
    }
    glBindBuffer(GL_ARRAY_BUFFER, p->vbo); LOGOPENGLERROR();
    glBufferData(GL_ARRAY_BUFFER, sizeof(CubeVerticesData), CubeVerticesData, GL_STATIC_DRAW); LOGOPENGLERROR();

    glGenBuffers(1, &p->indicesVbo); LOGOPENGLERROR();
    if (p->indicesVbo == 0) {
        LOG_ERROR("Unable to create Vertex Buffer Object for Skybox Vertex Indices");
        return -1;
    }
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, p->indicesVbo); LOGOPENGLERROR();
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(CubeIndicesData), CubeIndicesData, GL_STATIC_DRAW); LOGOPENGLERROR();
    p->indicesCount = ARRAY_SIZE(CubeIndicesData);

    GLint aCoord = glGetAttribLocation(p->program, "coord"); LOGOPENGLERROR();
    glEnableVertexAttribArray(aCoord); LOGOPENGLERROR();
    glVertexAttribPointer(aCoord, 3, GL_FLOAT, GL_FALSE, 0, 0); LOGOPENGLERROR();

    glBindVertexArray(0); LOGOPENGLERROR();

    return 0;
}

void Skybox_Release(Skybox* p) {
    assert(p);

    if (p->cubemapTex) {
        glDeleteTextures(1, &p->cubemapTex); LOGOPENGLERROR();
        p->cubemapTex = 0;
    }
    if (p->vbo) {
        glDeleteBuffers(1, &p->vbo); LOGOPENGLERROR();
        p->vbo = 0;
    }
    if (p->indicesVbo) {
        glDeleteBuffers(1, &p->indicesVbo); LOGOPENGLERROR();
        p->indicesVbo = 0;
    }
    if (p->vao) {
        glDeleteVertexArrays(1, &p->vao); LOGOPENGLERROR();
        p->vao = 0;
    }
    if (p->program != 0) {
        glDeleteProgram(p->program); LOGOPENGLERROR();
        p->program = 0;
    }
}

void Skybox_Render(const Skybox* p, const hmm_mat4 model, const hmm_mat4 view, const hmm_mat4 projection) {
    glUseProgram(p->program); LOGOPENGLERROR();
    glBindVertexArray(p->vao); LOGOPENGLERROR();

    glBindTexture(GL_TEXTURE_CUBE_MAP, p->cubemapTex); LOGOPENGLERROR();
    glActiveTexture(GL_TEXTURE0); LOGOPENGLERROR();
    glUniform1i(p->uCubeMap, 0); LOGOPENGLERROR();

    glUniformMatrix4fv(p->uModel, 1, GL_FALSE, (const GLfloat *)(&model)); LOGOPENGLERROR();
    glUniformMatrix4fv(p->uView, 1, GL_FALSE, (const GLfloat *)(&view)); LOGOPENGLERROR();
    glUniformMatrix4fv(p->uProjection, 1, GL_FALSE, (const GLfloat *)(&projection)); LOGOPENGLERROR();

    glDrawElements(GL_TRIANGLES, p->indicesCount, GL_UNSIGNED_SHORT, 0); LOGOPENGLERROR();

    glBindVertexArray(0); LOGOPENGLERROR();
    glUseProgram(0); LOGOPENGLERROR();
}
