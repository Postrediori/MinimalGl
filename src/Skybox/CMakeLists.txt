make_executable()

target_precompile_headers(${PROJECT} PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/pch.h)

target_include_directories(${PROJECT} PUBLIC
    ${OPENGL_INCLUDE_DIR}
    ${GLFW_INCLUDE_DIR}
    ${GLAD_INCLUDE_DIR}
    )

target_link_libraries(${PROJECT}
    ${GLFW_LIBRARIES}
    ${GLAD_LIBRARIES}
    ${HMM_LIBRARY}
    ${INIH_LIBRARIES}
    ${OPENGL_LIBRARIES}
    ${STB_LIBRARY}
    GraphicsUtilsLib
    UtilsLib
    )

# Data files
configure_file(
    ${CMAKE_SOURCE_DIR}/data/skybox.ini
    ${CMAKE_CURRENT_BINARY_DIR}/data/skybox.ini COPYONLY)
configure_file(
    ${CMAKE_SOURCE_DIR}/data/xpos.png
    ${CMAKE_CURRENT_BINARY_DIR}/data/xpos.png COPYONLY)
configure_file(
    ${CMAKE_SOURCE_DIR}/data/xneg.png
    ${CMAKE_CURRENT_BINARY_DIR}/data/xneg.png COPYONLY)
configure_file(
    ${CMAKE_SOURCE_DIR}/data/ypos.png
    ${CMAKE_CURRENT_BINARY_DIR}/data/ypos.png COPYONLY)
configure_file(
    ${CMAKE_SOURCE_DIR}/data/yneg.png
    ${CMAKE_CURRENT_BINARY_DIR}/data/yneg.png COPYONLY)
configure_file(
    ${CMAKE_SOURCE_DIR}/data/zpos.png
    ${CMAKE_CURRENT_BINARY_DIR}/data/zpos.png COPYONLY)
configure_file(
    ${CMAKE_SOURCE_DIR}/data/zneg.png
    ${CMAKE_CURRENT_BINARY_DIR}/data/zneg.png COPYONLY)
