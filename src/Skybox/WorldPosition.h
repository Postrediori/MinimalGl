#pragma once

typedef struct {
    int scrWidth, scrHeight;

    hmm_vec3 position;
    hmm_vec3 angle;

    hmm_vec3 up;
    hmm_vec3 right;
    hmm_vec3 forward;

    hmm_vec3 lookAt;
} WorldPosition;

void WorldPosition_SetPosition(WorldPosition* p, hmm_vec3 position, hmm_vec3 angle);
hmm_mat4 WorldPosition_GetViewMatrix(const WorldPosition* p);
void WorldPosition_SetCursorPos(WorldPosition* p, double x, double y);
void WorldPosition_ResizeScreen(WorldPosition* p, int width, int height);
