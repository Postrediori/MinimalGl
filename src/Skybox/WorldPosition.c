#include "pch.h"
#include "WorldPosition.h"

static const float RotationVel = 0.005;

static void WorldPosition_Update(WorldPosition* p) {
    assert(p);

    p->forward = HMM_Vec3(sin(p->angle.X), 0., cos(p->angle.X));

    p->right = HMM_Vec3(-cos(p->angle.X), 0., sin(p->angle.X));

    p->lookAt = HMM_Vec3(
        sin(p->angle.X) * cos(p->angle.Y),
        sin(p->angle.Y),
        cos(p->angle.X) * cos(p->angle.Y)
    );

    p->up = HMM_Cross(p->right, p->lookAt);
}

void WorldPosition_SetPosition(WorldPosition* p, hmm_vec3 position, hmm_vec3 angle) {
    assert(p);

    p->position = position;
    p->angle = angle;

    WorldPosition_Update(p);
}

hmm_mat4 WorldPosition_GetViewMatrix(const WorldPosition* p) {
    assert(p);

    hmm_vec3 posLookAt = HMM_AddVec3(p->position, p->lookAt);
    return HMM_LookAt(p->position, posLookAt, p->up);
}

void WorldPosition_SetCursorPos(WorldPosition* p, double x, double y) {
    assert(p);

    p->angle.X -= (x - (float)(p->scrWidth / 2)) * RotationVel;
    p->angle.Y -= (y - (float)(p->scrHeight / 2)) * RotationVel;

    // Boundary Conditions
    if (p->angle.X<-M_PI) {
        p->angle.X += M_PI * 2;
    }
    if (p->angle.X>M_PI) {
        p->angle.X -= M_PI * 2;
    }

    if (p->angle.Y<-M_PI_2) {
        p->angle.Y = -M_PI_2;
    }

    if (p->angle.Y>M_PI/2) {
        p->angle.Y = M_PI_2;
    }

    WorldPosition_Update(p);
}

void WorldPosition_ResizeScreen(WorldPosition* p, int width, int height) {
    assert(p);

    p->scrWidth = width;
    p->scrHeight = height;
}
