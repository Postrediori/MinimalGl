#pragma once

typedef struct {
    const char* xpos;
    const char* xneg;
    const char* ypos;
    const char* yneg;
    const char* zpos;
    const char* zneg;
} SkyboxConfiguration;

typedef struct {
    GLuint cubemapTex;
    GLuint program;
    GLint uModel, uView, uProjection, uCubeMap;
    GLuint vao;
    GLuint vbo;
    GLuint indicesVbo;
    size_t indicesCount;
} Skybox;

int Skybox_Init(Skybox* p, SkyboxConfiguration config);
void Skybox_Release(Skybox* p);

void Skybox_Render(const Skybox* p, const hmm_mat4 model, const hmm_mat4 view, const hmm_mat4 projection);
