#ifndef HANDMADE_MATH_IMPLEMENTATION
# define HANDMADE_MATH_IMPLEMENTATION
# include <HandmadeMath.h>
#endif

#include "pch.h"
#include "Utils.h"
#include "Logger.h"
#include "GraphicsLogger.h"
#include "Skybox.h"
#include "WorldPosition.h"


/*****************************************************************************
 * Window parameters
 ****************************************************************************/

static const int WindowWidth = 800;
static const int WindowHeight = 600;
static const char WindowTitle[] = "Skybox";

static const float ClearColor[] = {.15f, .15f, .15f, 1.f};

static const char SkyboxConfigFile[] = "data/skybox.ini";


/*****************************************************************************
 * Global variables
 ****************************************************************************/

static GLFWwindow *gWindow = NULL;
static bool gFullscreen = false;
static int gWindowWidth = 0, gWindowHeight = 0;

static Skybox gSkybox = {0};
static hmm_mat4 gProjection = {0};

static WorldPosition gPosition = {0};
static SkyboxConfiguration gSkyboxConfig = {0};


/*****************************************************************************
 * Callback forward definitions
 ****************************************************************************/

static void ErrorCallback(int e, const char *d);
static void KeyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
static void MouseMotionCallback(GLFWwindow* window, double x, double y);
static void MouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
static void ReshapeCallback(GLFWwindow* window, int width, int height);


/*****************************************************************************
 * Helper functions
 ****************************************************************************/

int InitWindow(GLFWwindow** pw, int width, int height, const char* title) {
    glfwSetErrorCallback(ErrorCallback);
    if (!glfwInit()) {
        LOG_ERROR("Failed to init GLFW");
        return -1;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // Required on Mac

    GLFWwindow* window = glfwCreateWindow(width, height, title, NULL, NULL);
    if (!window) {
        LOG_ERROR("Failed to initialize window with OpenGL 3.0 Core Profile");
        return -1;
    }

    *pw = window;

    glfwMakeContextCurrent(window);
    int err = gladLoadGL();
    if (err == 0){
        LOG_ERROR("Failed to initialize OpenGL loader");
        return -1;
    }

    enum { EnableVsync = 1, DisableVsync = 0 };
    glfwSwapInterval(DisableVsync);

    return 0;
}

void ToggleFullscreen(GLFWwindow* window, bool isFullscreen) {
    static struct {
        int savedXPos, savedYPos;
        int savedWidth, savedHeight;
    } info = {0};

    if (isFullscreen) {
        glfwGetWindowPos(window, &info.savedXPos, &info.savedYPos);
        glfwGetWindowSize(window, &info.savedWidth, &info.savedHeight);

        GLFWmonitor* monitor = glfwGetPrimaryMonitor();
        const GLFWvidmode* mode = glfwGetVideoMode(monitor);
        glfwSetWindowMonitor(window, monitor, 0, 0,
            mode->width, mode->height, mode->refreshRate);
    }
    else {
        glfwSetWindowMonitor(window, NULL,
            info.savedXPos, info.savedYPos,
            info.savedWidth, info.savedHeight, GLFW_DONT_CARE);
    }
}


/*****************************************************************************
 * Config loader
 ****************************************************************************/

static int SkyboxConfigLoader(void* user, const char* section,
        const char* name, const char* value) {
    SkyboxConfiguration* pconfig = (SkyboxConfiguration*)(user);

#define MATCH(s, n) (strcmp(section, s) == 0 && strcmp(name, n) == 0)
    if (MATCH("", "xpos")) {
        pconfig->xpos = strdup(value);
    }
    else if (MATCH("", "ypos")) {
        pconfig->ypos = strdup(value);
    }
    else if (MATCH("", "zpos")) {
        pconfig->zpos = strdup(value);
    }
    else if (MATCH("", "xneg")) {
        pconfig->xneg = strdup(value);
    }
    else if (MATCH("", "yneg")) {
        pconfig->yneg = strdup(value);
    }
    else if (MATCH("", "zneg")) {
        pconfig->zneg = strdup(value);
    }
    else {
        LOG_DEBUG("Unknown section=%s name=%s", section, name);
        return 0;
    }

    return 1;
}


/*****************************************************************************
 * Workflow functions
 ****************************************************************************/

int Init(int argc, const char** argv) {
    UNUSED(argc);
    UNUSED(argv);

    if (InitWindow(&gWindow, WindowWidth, WindowHeight, WindowTitle) != 0) {
        LOG_ERROR("Failed to init GLFW window");
        return -1;
    }

    glfwSetKeyCallback(gWindow, KeyboardCallback);
    glfwSetInputMode(gWindow, GLFW_STICKY_KEYS, GLFW_TRUE);

    glfwSetCursorPosCallback(gWindow, MouseMotionCallback);
    glfwSetMouseButtonCallback(gWindow, MouseButtonCallback);
    glfwSetInputMode(gWindow, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

    glfwSetWindowSizeCallback(gWindow, ReshapeCallback);

    LOG_INFO("OpenGL Renderer  : %s", glGetString(GL_RENDERER));
    LOG_INFO("OpenGL Vendor    : %s", glGetString(GL_VENDOR));
    LOG_INFO("OpenGL Version   : %s", glGetString(GL_VERSION));
    LOG_INFO("GLSL Version     : %s", glGetString(GL_SHADING_LANGUAGE_VERSION));

    if (ini_parse(SkyboxConfigFile, SkyboxConfigLoader, (void*)(&gSkyboxConfig)) < 0) {
        LOG_ERROR("Failed to init skybox config from file %s", SkyboxConfigFile);
        return -1;
    }

    if (Skybox_Init(&gSkybox, gSkyboxConfig) != 0) {
        LOG_ERROR("Failed to init skybox object");
        return -1;
    }

    WorldPosition_SetPosition(&gPosition,
        HMM_Vec3(0., 0., 0.),
        HMM_Vec3(1.92, 0.83, 0.));

    ReshapeCallback(gWindow, WindowWidth, WindowHeight);

    glClearColor(ClearColor[0], ClearColor[1], ClearColor[2], ClearColor[3]); LOGOPENGLERROR();
    glClearDepth(1.0); LOGOPENGLERROR();

    glEnable(GL_DEPTH_TEST); LOGOPENGLERROR();
    glDepthFunc(GL_LEQUAL); LOGOPENGLERROR();

    return 0;
}

void Release() {
    Skybox_Release(&gSkybox);
    if (gWindow != NULL) {
        glfwDestroyWindow(gWindow);
        gWindow = NULL;
    }
    glfwTerminate();
}

void Display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); LOGOPENGLERROR();

    static const float Scale = 50.;
    hmm_mat4 model = HMM_Scale(HMM_Vec3(Scale, Scale, Scale));

    hmm_mat4 view = WorldPosition_GetViewMatrix(&gPosition);

    Skybox_Render(&gSkybox, model, view, gProjection);
}

void Update(double t) {
    UNUSED(t);
}

/*****************************************************************************
 * Callbacks implementation
 ****************************************************************************/

static void ErrorCallback(int e, const char *d) {
    LOG_ERROR("GLFW Error %d: %s", e, d);
}

static void ReshapeCallback(GLFWwindow* window, int width, int height) {
    UNUSED(window);
    glViewport(0, 0, width, height);
    gWindowWidth = width;
    gWindowHeight = height;
    gProjection = HMM_Perspective(45.0f, (float)width/(float)height, 0.1f, 100.0f);
    WorldPosition_ResizeScreen(&gPosition, width, height);
}

void KeyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    UNUSED(scancode);
    UNUSED(mods);

    if (action == GLFW_PRESS) {
        switch (key) {
        case GLFW_KEY_ESCAPE:
            glfwSetWindowShouldClose(window, GLFW_TRUE);
            break;

        case GLFW_KEY_F1:
            gFullscreen = !gFullscreen;
            ToggleFullscreen(window, gFullscreen);
            break;
        }
    }
}

void MouseMotionCallback(GLFWwindow* window, double x, double y) {
    UNUSED(window);
    UNUSED(x);
    UNUSED(y);
    static bool warp = false;

    if (!warp) {
        WorldPosition_SetCursorPos(&gPosition, x, y);

        glfwSetCursorPos(window, gWindowWidth / 2, gWindowHeight / 2);
        warp = true;
    }
    else {
        warp = false;
    }
}

void MouseButtonCallback(GLFWwindow* window, int button, int action, int mods) {
    UNUSED(mods);
    if (button == GLFW_MOUSE_BUTTON_RIGHT) {
        if (action == GLFW_PRESS) {
            double x = 0., y = 0.;
            glfwGetCursorPos(window, &x, &y);

            // Mouse down
            (void)(x);
            (void)(y);
        }
        else if (action == GLFW_RELEASE) {
            // Mouse up
        }
    }
}


/*****************************************************************************
 * main
 ****************************************************************************/

int main(int argc, const char** argv) {
    if (Init(argc, argv) != 0) {
        return -1;
    }

    while (!glfwWindowShouldClose(gWindow)) {
        /* Input */
        glfwPollEvents();

        /* Draw */
        Display();

        Update(glfwGetTime());

        glfwSwapBuffers(gWindow);
    }

    Release();

    return 0;
}
